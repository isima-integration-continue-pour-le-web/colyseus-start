import { Room, Client } from "@colyseus/core";
import { MyRoomState } from "./schema/MyRoomState";

export class MyRoom extends Room<MyRoomState> {
  maxClients = 4;

  onCreate () {
    this.setState(new MyRoomState());

    this.onMessage("hello", () => {
      console.log("Someone sent Hello")
    });
  }

  onJoin (client: Client) {
    console.log(client.sessionId, "joined!");
  }

  onLeave (client: Client) {
    console.log(client.sessionId, "left!");
  }

  onDispose() {
    console.log("room", this.roomId, "disposing...");
  }

}
